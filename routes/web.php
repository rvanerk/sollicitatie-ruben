<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', ['as'=>'index', 'uses' => 'ItemController@index']);
$router->post('/create/item/', ['as'=>'itemCreate', 'uses' => 'ItemController@createItem']);
$router->post('/create/group/', ['as'=>'groupCreate', 'uses' => 'ItemController@createGroup']);
$router->get('/detail/item/{itemId}', ['as'=>'itemDetail', 'uses' => 'ItemController@itemDetail']);

