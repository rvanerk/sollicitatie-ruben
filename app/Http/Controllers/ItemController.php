<?php

namespace App\Http\Controllers;

use App\Group;
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index() {
        $topLevelGroups = Group::getTopLevelGroups();
        $allGroups = Group::all();
        $data = [
            'topLevelGroups' => $topLevelGroups,
            'allGroups' => $allGroups,
            'parent' => null,
            'orphans' => Item::orphans()
        ];
        return view('index')->with($data);
    }

    public function createItem(Request $request) {
        $name = $request->input('name');
        $groupInput = $request->input('group');
        $groupId = ($groupInput == '' ? null : $groupInput);
        $item = new Item([
            'name' => $name,
            'group_id' => $groupId
        ]);
        $item->save();
        return redirect()->route('index');
    }

    public function createGroup(Request $request) {
        $name = $request->input('name');
        $groupInput = $request->input('group');
        $groupId = ($groupInput == '' ? null : $groupInput);
        $item = new Group([
            'name' => $name,
            'group_id' => $groupId
        ]);
        $item->save();
        return redirect()->route('index');
    }

    public function itemDetail($itemId) {
        $item = Item::find($itemId);
        return $item->name;
    }
}
