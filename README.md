#Sollicitatie Ruben van Erk

## How to run

1. Enable pdo\_sqlite driver in php.ini by uncommenting the following line:
extension=pdo\_sqlite

2. Run: php -S localhost:8000 -t public

3. Go to localhost:8000

## Structure

Used framework: Lumen

* app/Http/Controllers contains controllers
* app contains the 2 models
* routes/web.php contains routes
* resources/views contains views
