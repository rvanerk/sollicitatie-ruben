<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{

    public function up()
    {
        Schema::create('groups', function(Blueprint $table) {
            $table->increments('id');
            // Schema declaration
            // Constraints declaration
            $table->string('name');
            $table->integer('group_id')->length(10)->unsigned()->nullable();
        });

        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    public function down()
    {
        Schema::drop('groups');
    }
}
