@extends('base')

@section('main')
    <ul>
        @include('groups', array('groups' => $topLevelGroups))

        @if(!$orphans->isEmpty())
            <li><strong>ungrouped</strong></li>
            <ul>
                @foreach($orphans as $orphan)
                    <li><a href="{{ route('itemDetail', ['itemId' => $orphan->id]) }}">{{$orphan->name}}</a></li>
                @endforeach
            </ul>
        @endif
    </ul>

    <div class="row">
        <div class="col-md-6">
            <h2>Create item</h2>
            <form action="{{ route('itemCreate') }}" method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="group">Group</label>
                    <select class="form-control" name="group" id="group">
                        <option value=""></option>
                        @foreach($allGroups as $group)
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="col-md-6">
            <h2>Create group</h2>

            <form action="{{ route('groupCreate') }}" method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="group">
                        Parent group
                    </label>
                    <select class="form-control" name="group" id="group">
                        <option value=""></option>
                        @foreach($allGroups as $group)
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@stop