<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

    protected $fillable = ['name', 'group_id'];

    public $timestamps = false;

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    // Relationships
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public static function orphans()
    {
        return Item::where('group_id', null)->get();
    }
}
