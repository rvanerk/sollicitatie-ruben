@foreach($groups as $group)
        <li class="group">
            <strong>{{ $group->name }}</strong>
            <ul>
                @foreach($group->items as $item)
                    <li class="item"><a href="{{ route('itemDetail', ['itemId' => $item->id]) }}">{{$item->name}}</a></li>
                @endforeach
            </ul>
            @if($group->children)
                <ul>
                    @include('groups', array('parent' => $group, 'groups' => $group->children))
                </ul>
            @endif
        </li>
@endforeach