<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

    protected $fillable = ['name', 'group_id'];

    public $timestamps = false;

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function items() {
        return $this->hasMany('App\Item');
    }

    public function children()
    {
        return $this->hasMany('App\Group', 'group_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    public static function getTopLevelGroups() {
        return Group::where('group_id', null)->get();
    }
}
